﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace homework.models
{
    public class WorkerFilterModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}
