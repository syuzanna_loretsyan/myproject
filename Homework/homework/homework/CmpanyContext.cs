﻿using homework.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace homework
{
    public class CompanyContext : DbContext
    {
        public CompanyContext()
        {

        }
        public CompanyContext(DbContextOptions options)
            :base(options)
        {

        }
        public DbSet<Worker> Workers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Worker>(x =>
            {
                x.ToTable("Workers");
                x.HasKey(x => x.Id);
                x.Property(x => x.Id).ValueGeneratedOnAdd();

                x.Property(x => x.Name).HasColumnType("NVARCHAR(25)");
                x.Property(x => x.Surname).HasColumnType("NVARCHAR(25)");
                x.Property(x => x.Phone).HasColumnType("NVARCHAR(15)");
                x.Property(x => x.Address).HasColumnType("NVARCHAR(50)");
            }
                );
            base.OnModelCreating(modelBuilder);
        }
    }
}
