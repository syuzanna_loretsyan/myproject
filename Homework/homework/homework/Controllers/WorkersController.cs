﻿using homework.models;
using homework.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using homework.Abstractions;

namespace homework.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WorkersController : ControllerBase
    {
        private readonly CompanyContext _context;
        private readonly IPrinter _printer;
        public WorkersController(CompanyContext context)//, IPrinter printer)
        {
            _context = context;
           // _printer = printer;
        }
        [HttpGet("print")]
        public IActionResult Print([FromQuery] string message)
        {
            _printer.Print(message);
            return Ok();
        }

        [HttpGet()]
        public async Task<IActionResult> GetWorkersAsync([FromQuery]WorkerFilterModel filter)
        {
                var query = _context.Workers.AsQueryable();

                if (filter.Id.HasValue)
                {
                    query = query.Where(x => x.Id == filter.Id);
                }

                if (!string.IsNullOrEmpty(filter.Name))
                {
                    query = query.Where(x => x.Name.ToLower().Contains(filter.Name.ToLower()));
                }

                var workers = await query.Select(x => new WorkerViewModel
                {
                    Id = x.Id,
                    Phone = x.Phone,
                    Surname = x.Surname,
                    Name = x.Name,
                    Address=x.Address
                }).ToArrayAsync();

                return Ok(workers);
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> RemoveStudentAsync([FromRoute] int id)
        {
                var worker = await _context.Workers.FirstOrDefaultAsync(x => x.Id == id);

                if (worker == null)
                {
                    return BadRequest();
                }

                _context.Workers.Remove(worker);
                await _context.SaveChangesAsync();

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> AddStudentAsync([FromBody] WorkerAddModel model)
        {
                var worker = new Worker
                {
                    Name = model.Name,
                    Surname = model.Surname,
                    Address=model.Address,
                    Phone=model.Phone
                };

                _context.Workers.Add(worker);
                await _context.SaveChangesAsync();

                return Created("", worker);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> AddStudentAsync([FromRoute] int id, [FromBody] WorkerEditModel model)
        {
                var workerToEdit = await _context.Workers.FirstOrDefaultAsync(x => x.Id == id);

                if (workerToEdit == null)
                {
                    return BadRequest();
                }

                workerToEdit.Name = string.IsNullOrEmpty(model.Name) ? workerToEdit.Name : model.Name;
                workerToEdit.Surname = string.IsNullOrEmpty(model.Surname) ? workerToEdit.Surname : model.Surname;
                workerToEdit.Address = string.IsNullOrEmpty(model.Address) ? workerToEdit.Address : model.Address;
                workerToEdit.Phone = string.IsNullOrEmpty(model.Phone) ? workerToEdit.Phone : model.Phone;

                _context.Workers.Update(workerToEdit);
                await _context.SaveChangesAsync();

            return Ok();
        }
    }
}
