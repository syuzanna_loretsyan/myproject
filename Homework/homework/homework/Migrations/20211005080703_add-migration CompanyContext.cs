﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace homework.Migrations
{
    public partial class addmigrationCompanyContext : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Workers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "NVARCHAR(25)", nullable: true),
                    Surname = table.Column<string>(type: "NVARCHAR(25)", nullable: true),
                    Phone = table.Column<string>(type: "NVARCHAR(15)", nullable: true),
                    Address = table.Column<string>(type: "NVARCHAR(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Workers", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Workers");
        }
    }
}
